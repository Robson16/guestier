<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>

    <?php
    // Se imagem de cabeçalho personalizável estiver selecionada 
    if( get_header_image() ) {
        $header_style = "background: url('" . get_header_image() . "'); ";
        $header_style .= "color: #" . get_header_textcolor() . ";";
    }  
    ?>

    <header id="header" style="<?php echo $header_style; ?>">
        <?php 
        if ( wp_is_mobile() ) :
            get_sidebar( 'nav' );
        else: 
        ?>
            <nav class="navbar navbar-expand-lg position-fixed w-100">
                <div class="container">
                    <div class="navbar-brand-wrapp">
                        <a class="navbar-brand hashtag" href="<?php echo get_home_url(); ?>">#<span>seu</span>guestier</a>
                    </div>

                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'main_menu',
                        'depth' => 2,
                        'container' => 'div',
                        'container_class' => 'collapse navbar-collapse',
                        'container_id' => 'navbar-nav',
                        'menu_class' => 'navbar-nav ml-auto',
                        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                        'walker' => new WP_Bootstrap_Navwalker()
                    ));
                    ?>
                </div>
                <!--/.container-->
            </nav>
        <?php endif; ?>

        <div class="container d-flex flex-column flex-lg-row align-items-center justify-content-between py-3">
            
            <?php if ( function_exists( 'the_custom_logo' ) ) the_custom_logo(); ?>                  
            
            <div class="d-flex flex-column justify-content-center justify-content-md-end align-self-lg-end align-items-center align-items-lg-end">
                <?php if ( is_active_sidebar( 'guestier-sidebar-header' ) ) dynamic_sidebar( 'guestier-sidebar-header' ); ?>
                <p class="hashtag mb-0">#<span>seu</span>guestier</p>
            </div>
            
        </div>
        <!-- /.container -->

    </header>