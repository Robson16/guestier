<?php

/**
 * Template for displaying search forms
 * 
 */
?>

<form role="search" method="get" class="search-form form-inline w-100" action="<?php echo esc_url( home_url( '/' ) ); ?>">

    <label class="search-label d-none" for="s"><?php _e('Search', 'guestier'); ?></label>
    <div class="input-group flex-grow-1">
        <input 
            type="search" 
            id="s" 
            name="s" 
            class="search-field form-control flex-grow-1" 
            placeholder="<?php _e('Procurar por', 'guestier') ?>" 
            value="<?php echo get_search_query(); ?>" 
            aria-label="<?php _e('Procurar', 'guestier'); ?>" 
        />
        <div class="input-group-append">
            <button class="search-submit" type="submit"></button>
        </div>
    </div>
</form>