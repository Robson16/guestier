<?php
/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>

<footer id="footer">
    <div class="container pt-5">
        <div class="row ">
            <div class="col-12 col-md-6 col-lg-3">
                <?php if ( is_active_sidebar( 'guestier-sidebar-footer-1' ) ) dynamic_sidebar( 'guestier-sidebar-footer-1' ); ?>
            </div>
            <!-- /.col -->
            <div class="col-12 col-md-6 col-lg-3">
                <?php if ( is_active_sidebar( 'guestier-sidebar-footer-2' ) ) dynamic_sidebar( 'guestier-sidebar-footer-2' ); ?>
            </div>
            <!-- /.col -->
            <div class="col-12 col-md-6 col-lg-3">
                <?php if ( is_active_sidebar( 'guestier-sidebar-footer-3' ) ) dynamic_sidebar( 'guestier-sidebar-footer-3' ); ?>
            </div>
            <!-- /.col -->
            <div class="col-12 col-md-6 col-lg-3">
                <?php if ( is_active_sidebar( 'guestier-sidebar-footer-4' ) ) dynamic_sidebar( 'guestier-sidebar-footer-4' ); ?>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <div class="container d-flex flex-column flex-lg-row align-items-center justify-content-between py-3">
        <span class="text-white">&copy; <?php echo bloginfo('title') . ', ' . wp_date('Y'); ?> - Todos os direitos reservados</span>
        <span class="text-white"><?php _e( 'Design por', 'guestier'); ?> <a href="http://www.stratesign.com.br/" target="_blank" rel="noopener">Stratesign</a></span>
    </div>
</footer>   

<?php wp_footer(); ?>

</body>

</html>