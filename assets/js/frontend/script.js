/**
 * Theme Main Js File
 */

jQuery(document).ready(function ($) {
    // Navbar
    var navbar = $('#header .navbar');
    var navbarHeight = navbar.height();

    $(window).scroll(function () {
        var smallLogoHeight = $('.navbar-brand').height();
        var bigLogoHeight = $('.custom-logo').height();

        var smallSpeed = (smallLogoHeight / bigLogoHeight);
        var ySmall = ($(window).scrollTop() * smallSpeed);

        var smallPadding = navbarHeight - ySmall;

        if (smallPadding > navbarHeight) {
            smallPadding = navbarHeight;
        }
        if (smallPadding < 0) {
            smallPadding = 0;
        }

        $('.navbar-brand-wrapp').css({
            "padding-top": smallPadding
        });

        var navScroll = ySmall / smallLogoHeight;

        if (navScroll >= 1) {
            navbar.addClass("is-navbar-dark");
        } else {
            navbar.removeClass("is-navbar-dark");
        }
    });

    // Sidebar Nav 
    $('.sidebar-nav-toggler').click(function () {
        $('#sidebar-nav').animate({
            width: 'toggle'
        });

        $('.sidebar-nav-container').toggleClass('is-open');
    });
});