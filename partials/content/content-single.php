<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'mb-4' ); ?>>
	<div class="row no-gutters">

		<div class="col-auto col-lg-2">
			<div class="text-uppercase text-center bg-white p-3">
				<strong class="mb-1"><?php echo get_the_date('d/m/y'); ?></strong>
				<br>
				<span class="small mb-0"><?php the_category(', '); ?></span>
			</div>
		</div>
		<!-- /.col -->

		<div class="col-12 col-lg-10">
			<figure title="<?php the_title_attribute(); ?>">
				<?php
				the_post_thumbnail( 'medium_large', array(
					'class' => 'img-fluid',
					'title' => get_the_title()
				) );
				?>
			</figure>

			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header>
			<!-- /.entry-header -->

			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<!-- /.entry-content -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</article><!-- #post-<?php the_ID(); ?> -->