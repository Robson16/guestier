<?php
/*
 * Template Part to display that no posts were found
 */
?>

<div class="col">
    <h3 class="text-center"><?php _e('Nenhum conteúdo para exibir', 'guestier'); ?></h3>
</div>
