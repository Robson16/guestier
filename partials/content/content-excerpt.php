<?php

/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('mb-4'); ?>>
	<div class="row no-gutters">

		<div class="col-auto col-lg-2">
			<div class="text-uppercase text-center bg-white p-3">
				<strong class="mb-1"><?php echo get_the_date('d/m/y'); ?></strong>
				<br>
				<span class="small mb-0"><?php the_category(', '); ?></span>
			</div>
		</div>
		<!-- /.col -->

		<div class="col-12 col-lg-10">
			<figure title="<?php the_title_attribute(); ?>">
				<a href="<?php the_permalink(); ?>">
					<?php
					the_post_thumbnail('medium_large', array(
						'class' => 'img-fluid',
						'title' => get_the_title()
					));
					?>
				</a>
			</figure>

			<header class="entry-header">
				<?php
				if (is_sticky() && is_home() && !is_paged()) {
					printf('<span class="sticky-post">%s</span>', _x('Post', 'Destaque', 'guestier'));
				}
				the_title(sprintf('<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h3>');
				?>
			</header>
			<!-- /.entry-header -->

			<div class="entry-content">
				<p><?php echo wp_trim_words( get_the_content(), 35 ); ?></p>
			</div>
			<!-- /.entry-content -->

			<footer class="entry-footer">
				<a class="read-more-link" href="<?php echo esc_url(get_permalink()); ?>"><?php _e('Ler Mais', 'guestier') ?></a>
			</footer>
			<!-- /.entry-footer -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</article><!-- #post-<?php the_ID(); ?> -->