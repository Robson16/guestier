<?php

/**
 *  Theme Main Mobile Menu Sidebar
 * 
 */

?>

<aside class="sidebar-nav-container">

    <button class="sidebar-nav-toggler" type="button">
        <span class="sidebar-nav-toggler-icon"></span>
    </button>

    <?php
    wp_nav_menu(array(
        'theme_location' => 'main_menu',
        'depth' => 2,
        'container' => 'nav',
        'container_class' => 'collapse sidebar-nav-collapse',
        'container_id' => 'sidebar-nav',
        'menu_class' => 'sidebar-nav',
    ));
    ?>

</aside>