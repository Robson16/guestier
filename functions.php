<?php

/**
 * Petra Land-Page funções e definições
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

/**
 * Enfileiramento de scripts e styles.
 */
function guestier_scripts() {
    // CSS
    wp_enqueue_style( 'bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', null, '4.3.1', 'all' );
    wp_enqueue_style('guestier-shared-style', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get('Version'));
    wp_enqueue_style('guestier-frontend-style', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array('bootstrap'), wp_get_theme()->get('Version'));

    // Js
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', '//code.jquery.com/jquery-3.4.1.min.js', array(), '3.4.1', true );
    wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array( 'jquery' ), '4.3.1', true );
    wp_enqueue_script( 'ekko-lightbox', '//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js', array( 'jquery', 'bootstrap' ), '5.3.0', true );
    wp_enqueue_script( 'guestier-frontend-script', get_template_directory_uri() . '/assets/js/frontend/script.js', array( 'jquery' ), wp_get_theme()->get( 'Version' ), true );
}

add_action('wp_enqueue_scripts', 'guestier_scripts');

/**
 * Configura os padrões do tema e registra o suporte para vários recursos do WordPress.
 */
if (!function_exists('guestier_setup')) {

    function guestier_setup() {

        // Habilitando o suporte à tradução
        $textdomain = 'guestier';
        load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages/');
        load_theme_textdomain($textdomain, get_template_directory() . '/languages/');

        // Registro de menus
        register_nav_menus(array(
            'main_menu' => __('Menu Principal', 'guestier'),
        ));

        // Deixe o WordPress gerenciar o título do documento.
        add_theme_support('title-tag');
       
		// Ative o suporte para imagem destacada de postagem em postagens e páginas.		 	
        add_theme_support( 'post-thumbnails' );

        // Carregar estilos personalizados no editor.
        add_theme_support( 'editor-styles' );
        add_editor_style( get_stylesheet_directory_uri() . '/assets/css/shared/style.css' );

        // Ativa suporte a mideas incorporadas para full wight
        add_theme_support( 'responsive-embeds' );

        // Ativa dimensões wide e full
        add_theme_support( 'align-wide' );

        // Desabilida cores personalidas
        add_theme_support( 'disable-custom-colors' );
        // Cria a paleta de cores especifica 
        add_theme_support('editor-color-palette', array(
            array(
                'name'  => __('Branco', 'guestier'),
                'slug'  => 'white',
                'color'    => '#ffffff',
            ),            
            array(
                'name'  => __('Platina', 'guestier'),
                'slug'  => 'platinum',
                'color'    => '#e9e9e9',
            ),
            array(
                'name'  => __('Cinza', 'guestier'),
                'slug'  => 'gray',
                'color'    => '#bfbdbe',
            ),            
            array(
                'name'  => __('Amarelo Brilhante', 'guestier'),
                'slug'  => 'bright-yellow',
                'color' => '#fab718',
            ),
            array(
                'name'  => __('Urobilina', 'guestier'),
                'slug'  => 'urobilin',
                'color' => '#ecaf2f',
            ),
            array(
                'name'    => __('Roxo Escuro', 'guestier'),
                'slug'    => 'dark-purple',
                'color'    => '#3b1e32',
            ),
            array(
                'name'    => __('Preto Uva Passa', 'guestier'),
                'slug'    => 'raisin-black',
                'color'    => '#251120',
            ),
            array(
                'name'    => __('Alcaçuz', 'guestier'),
                'slug'    => 'licorice',
                'color'    => '#1c0514',
            ),
            array(
                'name'  => __('Preto', 'guestier'),
                'slug'  => 'black',
                'color'    => '#000000',
            ),
        ));

        // Desabilita gradientes personalizados
        add_theme_support( 'disable-custom-gradients' );
        // Adiciona esquemas de gradientes
        add_theme_support(
            'editor-gradient-presets',
            array(
                array(
                    'name'     => __( 'Amarelo Brilhante - Platina', 'guestier' ),
                    'gradient' => 'linear-gradient(160deg, #fab718 0%, #fab718 50%, #e9e9e9 50%, #e9e9e9 100%)',
                    'slug'     => 'bright-yellow-to-platinum'
                ),
                array(
                    'name'     => __( 'Platina - Amarelo Brilhante', 'guestier' ),
                    'gradient' => 'linear-gradient(160deg, #e9e9e9 0%, #e9e9e9 50%, #fab718 50%, #fab718 100%)',
                    'slug'     => 'platinum-to-bright-yellow'
                ),
                array(
                    'name'     => __( 'Amarelo Brilhante - Preto Uva Passa', 'guestier' ),
                    'gradient' => 'linear-gradient(160deg, #fab718 0%, #fab718 50%, #251120 50%, #251120 100%)',
                    'slug'     => 'bright-yellow-to-raisin-black'
                ),
                array(
                    'name'     => __( 'Preto Uva Passa - Amarelo Brilhante', 'guestier' ),
                    'gradient' => 'linear-gradient(160deg, #251120 0%, #251120 50%, #fab718 50%, #fab718 100%)',
                    'slug'     => 'raisin-black-to-bright-yellow'
                ),
                array(
                    'name'     => __( 'Platina - Preto Uva Passa', 'guestier' ),
                    'gradient' => 'linear-gradient(160deg, #e9e9e9 0%, #e9e9e9 50%, #251120 50%, #251120 100%)',
                    'slug'     => 'platinum-to-raisin-black'
                ),
                array(
                    'name'     => __( 'Preto Uva Passa - Platina', 'guestier' ),
                    'gradient' => 'linear-gradient(160deg, #251120 0%, #251120 50%, #e9e9e9 50%, #e9e9e9 100%)',
                    'slug'     => 'raisin-black-to-platinum'
                ),
            )
        );

        // Logo personalizavel
        add_theme_support( 'custom-logo', array(
            'height'      => 200,
            'width'       => 300,
            'flex-height' => true,
            'flex-width'  => true,
            'header-text' => array( 'site-title', 'site-description' ),
        ) );

        // Imagem de cabeçalho personalizavel
        add_theme_support( 'custom-header', array(
            'width'              => 2560,
            'height'             => 490,
            'flex-width'         => true,
            'flex-height'        => true,
        ) );

    }

}

add_action( 'after_setup_theme', 'guestier_setup' );

/**
 * Registro de áreas de widgets.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function guestier_sidebars() {

    // Argumentos usados em todas as chamadas register_sidebar ().
    $shared_args = array(
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
        'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
        'after_widget' => '</div></div>',
    );

    // Rodapé #1
    register_sidebar( array_merge( $shared_args, array(
        'name' => __('Cabeçalho', 'guestier'),
        'id' => 'guestier-sidebar-header',
        'description' => __('Os widgets nesta área serão exibidos á direita no cabeçalho.', 'guestier'),
    ) ) );
    
    // Rodapé #1
    register_sidebar( array_merge( $shared_args, array(
        'name' => __('Rodapé #1', 'guestier'),
        'id' => 'guestier-sidebar-footer-1',
        'description' => __('Os widgets nesta área serão exibidos na primeira coluna no rodapé.', 'guestier'),
    ) ) );

    // Rodapé #2
    register_sidebar( array_merge( $shared_args, array(
        'name' => __('Rodapé #2', 'guestier'),
        'id' => 'guestier-sidebar-footer-2',
        'description' => __('Os widgets nesta área serão exibidos na segunda coluna no rodapé.', 'guestier'),
    ) ) );

    // Rodapé #3
    register_sidebar(array_merge($shared_args, array(
        'name' => __('Rodapé #3', 'guestier'),
        'id' => 'guestier-sidebar-footer-3',
        'description' => __('Os widgets nesta área serão exibidos na terceira coluna no rodapé.', 'guestier'),
    )));

    // Rodapé #4
    register_sidebar(array_merge($shared_args, array(
        'name' => __('Rodapé #4', 'guestier'),
        'id' => 'guestier-sidebar-footer-4',
        'description' => __('Os widgets nesta área serão exibidos na quarta coluna no rodapé.', 'guestier'),
    )));

    // Barra Lateral Blog #1
    register_sidebar( array_merge( $shared_args, array (
        'name' => __( 'Barra Lateral Widgets #1', 'guestier' ),
        'id' => 'guestier-sidebar-blog-1',
        'description' => __( 'Os widgets nesta área serão exibidos em uma coluna cinza.', 'guestier' ),
    ) ) );

    // Barra Lateral Blog #2
    register_sidebar( array_merge( $shared_args, array (
        'name' => __( 'Barra Lateral Widgets #2', 'guestier' ),
        'id' => 'guestier-sidebar-blog-2',
        'description' => __( 'Os widgets nesta área serão exibidos abaixo da coluna cinza.', 'guestier' ),
    ) ) );
}

add_action( 'widgets_init', 'guestier_sidebars' );

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 *  TGM Plugin Configurações
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 * Funções, filtros e ações extras para o tema
 */
require get_template_directory() . '/includes/template-functions.php';
