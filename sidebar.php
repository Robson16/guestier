<?php

/**
 * Sidebar Blog
 * 
 */
?>

<aside class="sidebar-blog">
    <div class="widgets-group">
        <?php if ( is_active_sidebar( 'guestier-sidebar-blog-1' ) ) dynamic_sidebar( 'guestier-sidebar-blog-1' ); ?>
    </div>

    <div class="widgets-group">
        <?php if ( is_active_sidebar( 'guestier-sidebar-blog-2' ) ) dynamic_sidebar( 'guestier-sidebar-blog-2' ); ?>
    </div>
</aside>