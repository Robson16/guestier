<?php
/**
 * O modelo para exibir páginas 404 (não encontrado)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 */
get_header();
?>

<main>
    <section class="container d-flex flex-column justify-content-center" style="min-height: 100vh;">
        <h1 class="page-title"><?php _e('Não é possível encontrar essa página', 'guestier'); ?></h1>
        <p><?php _e('Parece que nada foi encontrado neste local', 'guestier'); ?></p>
        <a class="h2 text-color-five font-weight-bold" href="<?php echo get_home_url(); ?>">Volte para página inicial</a>
    </section>
</main>

<?php
get_footer();
